(function($) {
	/* jshint strict:true */
	"use strict";
	var plugin,
		fns = {};

	$.widget('pg.validator', {
		options: {
			data: {
				email: "^[a-zA-Z0-9\\.\\-_]+@[a-zA-Z0-9\\-_]+\\.[a-zA-z]{1,3}(\\.[a-zA-Z]{1,2})*$",
				text: "",
				number: "^\\d+$",
				currency: "^[+-]?[0-9]{1,3}(?:\\.?[0-9]{3})*,[0-9]{2}$",
				date: "^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)(?:0?2|(?:Feb))\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$"
			},
			requiredClass: 'required',
			validatorClass: 'validator',
			errorClass: 'invalid',
			button: "",
			onSuccess: function() {},
			onFailure: function() {}
		},
		_create: function () {
			var self = this,
				elementQuery = this._format("input.{0}, textarea.{0}", this.options.requiredClass); // mount the query for the elements to our validation

			this._IsValid = false;

			this._validator = self.element;
			this._validator.find(elementQuery).each(function() {
				var $field = $(this),
					type = $field.attr("data-type");
					
				if(type === 'text'){
					$field.focusout(function(event) {
						if(self._IsValid) {
							self._IsValid = self._isText(this, event);
						} else {
							self._isText(this, event);
						}
					});
				}
				else {
					$field.focusout(function(event) {
						if(self._IsValid) {
							self._IsValid = self._isExpression(this, { e: event, expression: self.options.data[type] });
						} else {
							self._isExpression(this, { e: event, expression: self.options.data[type] });
						}
					});
				}
			});

			$('#pg-validator').css('display', 'none');

			// Check if a buton was chosen
			if(typeof self.options.button !== "undefined" && self.options.button !== "" && self.options.button !== null) {
				
				
				if(typeof self.options.button === "string") {
					self._button = $("#" + self.options.button);
				} else {
					self._button = self.options.button;
				}

				if(self._button instanceof $ && self._button.length > 0) {
					self._button.click(function(event) {
						self.validate();
					});
				} else {
					self._validator.append(function() {
						return $("<div id='pg-validator' class='" + self.options.validatorClass + "'  >You must either pass a valid ID or jQuery button instance.</div>");
					});
					$('#pg-validator').css('display', 'block');
				}
			} 

			return self._validator;
		},
		/**
		 * Validate all required fields inside of the selected form
		 * @return {bool}
		 */
		validate: function() {
			this._IsValid = true;

			// Trigger all the validators and check if there are some errors
			this._validator.find('input.required, textarea.required').focusout();

			if(this._IsValid) {
				this.options.onSuccess();
			} else {
				this.options.onFailure();
			}
		},
		_isExpression: function(sender, args) {
			var $input = $(sender),
				valid = this._isText(sender, args),
				regex = new RegExp(args.expression, "g");

			valid = regex.test($input.val());

			this._checkWarnings(sender, { condition: valid });

			return valid;
		},
		_isText: function(sender, args) {
			var $input = $(sender),
				value = $input.val(),

				valid = typeof value !== "undefined" && value !== null && value !== "" && value.length > 0;

			this._checkWarnings(sender, { condition: valid });

			return valid;
		},
		_checkWarnings: function(sender, args) {
			var $input = $(sender);

			if(args.condition) {
				$input.parent().find('.' + this.options.validatorClass).css("display", "none");
				$input.removeClass(this.options.errorClass);
			} else {
				$input.parent().find('.' + this.options.validatorClass).css("display", "block");
				$input.addClass(this.options.errorClass);
			}
		},
		_format: function () {
			var output = arguments[0],
				regExp;

			for(var i = 1; i < arguments.length; i++) {
				regExp = new RegExp("\\{" + (i - 1) + "\\}", "gm");
				output = output.replace(regExp, arguments[i]);
			}

			return output;
		},
		_destroy: function() {
			return this._super();
		},
		_setOption: function(key, value) {
			var self = this,
				prev = this.options;
				// map functions later (if necessary)
				// map = { ... }
			this._super(key, value);
		}
	});
})(jQuery);