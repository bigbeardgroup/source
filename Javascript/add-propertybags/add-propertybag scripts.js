(function() {
	var parentSite = _spPageContextInfo.siteAbsoluteUrl,
		sites = {
			'en': [ parentSite + '/en' ],
					
			'es': [ parentSite + '/es' ],
					
			'pt': [ parentSite + '/pt' ]
		},
		language,
		site;

	// Get Client Context for all sub-sites
	for (language in sites) {
		if (sites.hasOwnProperty(language)) {
			if(sites[language].length > 1) {
				for(var item in sites[language]) {
					getClientContext(sites[language][item], language);
				}
			}
			else {
				getClientContext(sites[language], language);
			}
		}
	}

	function getClientContext(url, language) {
		var context = new SP.ClientContext(url),
			webSite = context.get_web(),
			properties = webSite.get_allProperties(),

			props = {
				'en': {
					"VariationLabel": "en"
				},
				'es': {
					"VariationLabel": "es"
				},
				'pt': {
					"VariationLabel": "pt"
				}
			},
			propSet,
			prop;

		for (prop in props[language]) {
			if (props[language].hasOwnProperty(prop)) {
				propSet = props[language];
				properties.set_item(prop, propSet[prop]);
			}
		}

		webSite.update();
		context.load(webSite);
		context.executeQueryAsync(successHandler, failureHandler);

	}

	function successHandler(data) {
		console.log(data);
	}

	function failureHandler(error) {
		console.log("Failure: " + error.get_message());
	}
})();