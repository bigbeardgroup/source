﻿using Microsoft.SharePoint.Utilities;
using System;
using System.Web.UI.WebControls.WebParts;

namespace SPLibrary.Entity
{
    class SecureWebPart : WebPart
    {
        /// <summary>
        /// OnInit event to validate form digest to avoid impersonate atempts
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected override void OnInit(EventArgs e)
        {
            if (Page.IsPostBack)
            {
                SPUtility.ValidateFormDigest();
            }
            base.OnInit(e);
            
        }
    }
}
