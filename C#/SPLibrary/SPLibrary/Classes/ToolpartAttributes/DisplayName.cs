﻿using SPLibrary.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls.WebParts;

namespace SPLibrary.Classes.ToolpartAttributes
{
    /// <summary>
    /// Custom Web Display Name Attribure for ToolPart
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Event)]
    class DisplayName : WebDisplayNameAttribute
    {
        public DisplayName(string key) 
            : base(ResourceManager.GetResourceByKey<Resource>(key))
        {

        }
    }
}
