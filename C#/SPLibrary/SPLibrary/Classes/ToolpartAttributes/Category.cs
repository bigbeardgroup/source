﻿using SPLibrary.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPLibrary.Classes.ToolpartAttributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Event)]
    class Category : CategoryAttribute
    {
        public Category(string key)
            : base(ResourceManager.GetResourceByKey<Resource>(key))
        {

        }
    }
}
