﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPLibrary.Classes
{
    public abstract class Resource
    {
        /// <summary>
        /// Gets the resource file name
        /// </summary>
        public const string FILE = "Resource";
    }
}
